const Express = require('express');
const bodyParser = require('body-parser');

const PORT = '8080';

const App = Express();

App.use(bodyParser.json());

App.use(bodyParser.urlencoded({ extended: false }));

App.use(Express.static('./dist'));
App.use(Express.static('./public'));

App.get('/something', (req, res) => {
  res.json({ result: 1000, q: req.query });
});

App.post('/something', (req, res) => {
  res.json({ result: 'foo-bar', b: req.body });
});

App.listen(PORT);
