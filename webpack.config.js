const HTMLWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        },
      },
      {
        test: /\.html$/,
        use: {
          loader: 'html-loader'
        }
      },
      { // sass loader
        test: /\.(sass|scss)$/,
        use: [{
          loader: 'style-loader', // creates style nodes from JS strings
          options: { modules: true },
        }, {
          loader: 'css-loader', // translates CSS into CommonJS
          options: { modules: true },
        }, {
          loader: 'sass-loader', // compiles Sass to CSS
          options: { modules: true },
        }],
      }
    ]
  },
  plugins: [
    new HTMLWebpackPlugin({
      template: './src/index.html',
      filename: './index.html',
    })
  ],
};