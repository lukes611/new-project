import React from 'react';
import ReactDom from 'react-dom';
import axios from 'axios';
import style from './index.sass';

class Page extends React.Component {
  async componentDidMount() {
    const { data } = await axios.get('/something?a=100&b=luke');
    const { data: data2 } = await axios.post('/something', { f: -20, y: 'yes!' });
    console.log(data, data2);
  }
  render() {
    return <div className={style.prim}>
      hello-react
      <img width="100" src="canva.png" />
    </div>;
  }
}

const element = document.getElementById('react-app');
ReactDom.render(<Page />, element);